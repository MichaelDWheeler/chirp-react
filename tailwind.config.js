module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      animation: {
        'fade-out': 'fade-out 0.5s linear',
      },
      backgroundColor: {
        'gray-250': '#f5f5f7',
      },
      borderColor: {
        'gray-250': '#e6e6e6',
      },
      flex: {
        '40': '0 0 40%',
        '50': '0 0 50%',
        '60': '0 0 60%',
      },
      fontFamily: {
        'proxima-nova': ['proxima-nova', 'sans-serif'],
      },
      fontSize: {
        '8xl': '6rem',
      },
      height: {
        'screen-double': '200vh',
        'screen-150': '150vh',
      },
      inset: {
        '80': '80px',
      },
      textColor: {
        'purple-650': '#6656e9',
      },
      margin: {
        '248': '248px',
        '255': '255px',
      },
      minHeight: {
        '1/4vh': '25vh',
        '2500': '2500px',
      },
      width: {
        '360': '360px',
        '400': '400px'
      }
    },
  },
  variants: {},
  plugins: [],
}
