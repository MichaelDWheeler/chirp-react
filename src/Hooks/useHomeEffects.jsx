import { useState, useRef } from 'react'

export default function useHomeEffects() {
    const [bottomPos, setPos] = useState(null)
    const [showH2, setH2Visible] = useState(true)
    const [top, setTop] = useState(300)
    const [showVideoSideMessage, setVideoSideMessage] = useState(false)

    const desktopRef = useRef(null)
    const videoRef = useRef(null)
    const h2Ref = useRef(null)
    const videoSideMessageRef = useRef(null)

    function getDesktopBottom() {
        let desktopBottom = desktopRef.current.getBoundingClientRect().bottom
        setPos(desktopBottom)
    }

    function videoRightSide() {
        return videoRef.current.getBoundingClientRect().right
    }

    function videoH2Status() {
        let h2RightSide = h2Ref.current.getBoundingClientRect().right
        if ((videoRightSide()) <= h2RightSide) {
            setH2Visible(false)
        } else {
            setH2Visible(true)
        }
    }

    function displayVideoSideMessage() {
        let windowHeight = window.innerHeight
        const videoSideMessageProps = videoSideMessageRef.current.getBoundingClientRect()
        let textHeight = videoSideMessageProps.height
        let centerPoint = (windowHeight - textHeight) / 2
        let leftSide = videoSideMessageProps.left
        setTop(centerPoint)
        if (leftSide >= (videoRightSide()) + 30) {
            setVideoSideMessage(true)
        } else {
            setVideoSideMessage(false)
        }
    }

    return {
        bottomPos,
        setPos,
        desktopRef,
        getDesktopBottom,
        showH2,
        videoRef,
        h2Ref,
        videoRightSide,
        videoH2Status,
        displayVideoSideMessage,
        top,
        showVideoSideMessage,
        videoSideMessageRef
    }
}