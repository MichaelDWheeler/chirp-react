import React from 'react'
import ButtonDemo from '../Components/ButtonDemo'
import Heading1 from '../Components/Heading1'
import Laptop from '../Components/Laptop'
import Desktop from '../Components/Desktop'
import ScalingVideo from '../Components/ScalingVideo'
import Competitive from '../Components/Competitive'
import Unrivaled from '../Components/Unrivaled'
import Conquesting from '../Components/Conquesting'
import Integrations from '../Components/Integrations'
import Footer from '../Components/Footer'

function Home(props) {
    return (
        <>
            <div className="bg-white pt-40">
                <Heading1 />
                <div className="flex justify-center w-full my-6">
                    <ButtonDemo open={props.open} />
                </div>
                <div className="flex justify-center w-full my-6 pb-32">
                    <Laptop />
                </div>
            </div>
            <Desktop
                desktopRef={props.desktopRef}
                setPos={props.setPos} />
            <ScalingVideo
                h2Ref={props.h2Ref}
                videoRef={props.videoRef}
                desktopBottom={props.desktopBottom}
                showH2={props.showH2}
                videoSideMessageRef={props.videoSideMessageRef}
                top={props.top}
                showVideoSideMessage={props.showVideoSideMessage} />
            <Competitive />
            <Unrivaled />
            <Conquesting />
            <Integrations open={props.open} />
            <Footer open={props.open} />
        </>
    )
}

export default Home