import React from 'react'
import laptop from '../Images/laptop.png'

function Laptop() {
    return (
        <img className="self-center" src={laptop} alt="laptop with offers" />
    )
}

export default Laptop