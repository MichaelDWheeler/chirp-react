import React from 'react'

function UnrivaledLeft() {
    return (
        <div className="flex w-full sm:w-1/2 items-center justify-end">
            <div className="w-full sm:w-360 ml-4 sm:mr-16 lg:mr-32 xl:mr-255">
                <h2 className="hidden sm:block sm:block text-3xl md:text-5xl font-bold font-proxima-nova leading-tight text-left text-black">Access Unrivaled First Party Data</h2>
                <p className="mt-8 sm:mt-0 text-center sm:text-left">Refine your ecommerce and Shopify strategy based on enriched data that goes beyond your brand's domain.</p>
            </div>
        </div>
    )
}

export default UnrivaledLeft