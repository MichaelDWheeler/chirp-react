import React, { useEffect, useState } from 'react'
import { useMediaQuery } from 'react-responsive'
import Video from '../Images/test__video-full-color.mp4'
import Poster from "../Images/video_poster.png"

function VideoExtensions(props) {

    const isMobile = useMediaQuery({
        query: '(max-width: 640px)'
    })

    const [control, setControl] = useState(false)

    useEffect(() => {

        function playVideo() {
            const playPromise = props.videoRef.current.play()
            if (playPromise !== undefined) {
                playPromise.then()
                    .catch(error => {
                        console.log(error)
                    })
            }
        }

        function pauseVideo() {
            props.videoRef.current.pause()
        }

        setControl(false)
        if (isMobile) {
            setControl(true)
        } else if (!props.showH2) {
            playVideo()
        } else {
            pauseVideo()
        }

    }, [isMobile, props.showH2, props.videoRef])

    let dynamicScaling = {
        transform: `matrix(${props.scale}, 0, 0, ${props.scale}, 0, 0)`,
    }

    return (
        <div className="relative px-4">
            <video ref={props.videoRef} style={dynamicScaling} muted controls={control} className="video focus:outline-none" poster={Poster}>
                <source src={Video} alt="Video showing extensions" type="video/mp4" />
            </video>
        </div>
    )
}

export default VideoExtensions