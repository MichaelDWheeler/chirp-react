import React from 'react'
import BrowserDiscount from './BrowserDiscount'

function CompetitiveLeft() {
    return (
        <div className="flex flex-wrap w-full sm:w-1/2 justify-end px-4">
            <h2 className="block sm:hidden w-full text-3xl font-bold font-proxima-nova leading-tight text-center mt-20 mb-8">Gain a New Competitive Edge</h2>
            <BrowserDiscount />
        </div>
    )
}

export default CompetitiveLeft