import React from 'react'
import CompetitiveLeft from './CompetitiveLeft'
import CompetitiveRight from './CompetitiveRight'

function Competitive() {
    return (
        <div className="pb-20 sm:py-40 bg-gray-250 flex flex-wrap overflow-hidden">
            <div className="flex flex-wrap w-full">
                <CompetitiveLeft />
                <CompetitiveRight />
            </div>

        </div>
    )
}

export default Competitive