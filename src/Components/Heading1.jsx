import React from 'react'

function Heading1() {
    return (
        <div className="flex justify-center w-full bg-white">
            <h1 className="max-w-screen-lg text-center text-3xl sm:text-3lg md:text-4xl lg:text-5xl xl:text-6xl 
            font-proxima-nova font-bold leading-tight">Empower Your Brand with an Intelligent Shopping Assistant.</h1>
        </div>
    )
}

export default Heading1