import React from 'react'
import FeaturedVendors from "../Images/vendors.png"

function Vendors() {
    return (<img className="mx-auto md:mx-0 mb-10 self-center" src={FeaturedVendors} alt="Top Brands" />)
}

export default Vendors