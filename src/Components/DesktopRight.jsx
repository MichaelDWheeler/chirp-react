import React from 'react'
import IconShopping from './IconShopping'
import IconDesktop from './IconDesktop'


function DesktopRight() {
    return (
        <div className="flex w-full md:w-1/2 justify-center md:justify-end font-proxima-nova text-lg">
            <div className="flex flex-wrap justify-center max-w-lg w-full content-start">
                <h3 className="w-full text-center text-2xl font-semibold leading-8">Leverage the Power of Desktop</h3>
                <div className="flex flex-wrap justify-center items-center w-full sm:w-1/2 mt-12">
                    <div className="w-full text-center flex justify-center">
                        <IconShopping />
                    </div>
                    <div className="w-full text-center flex justify-center">
                        <p className="my-4">Shoppers Are</p>
                    </div>
                    <div className="w-full text-center flex justify-center">
                        <p className="text-3xl sm:text-8xl leading-none">2.8x</p>
                    </div>
                    <div className="w-full text-center flex justify-center">
                        <p className="my-4">more likely to convert</p>
                    </div>
                </div>
                <div className="flex flex-wrap justify-center items-center w-full sm:w-1/2 mt-12">
                    <div className="w-full text-center flex justify-center">
                        <IconDesktop />
                    </div>
                    <div className="w-full text-center flex justify-center">
                        <p className="my-4">Average order value is</p>
                    </div>
                    <div className="w-full text-center flex justify-center">
                        <p className="text-3xl sm:text-8xl leading-none">48%</p>
                    </div>
                    <div className="w-full text-center flex justify-center">
                        <p className="my-4">higher than on mobile</p>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default DesktopRight