import React, { createRef, useState, useEffect } from 'react'
import { useTransition, animated } from 'react-spring'
import { useMediaQuery } from 'react-responsive'
import Image from "../Images/browser_Discount.png"

function BrowserDiscount() {

    const isMobile = useMediaQuery({
        query: '(max-width: 640px)'
    })

    const [show, set] = useState(false)

    const imageRef = createRef()

    const transitions = useTransition(show, null, {
        from: { transform: "translateX(-100%)" },
        enter: { transform: "translateX(0)" },
        leave: { transform: "translateX(-100%)" },
    })

    let jsxSection

    if (isMobile) {
        jsxSection = <img className="self-center" src={Image} alt="Browser displaying discounts" />
    } else {
        jsxSection =
            <div ref={imageRef} className="min-h-1/4vh">
                {
                    transitions.map(({ item, key, props }) =>
                        item && <animated.div key={key} style={props}><img className="self-center" src={Image} alt="Browser displaying discounts" /></animated.div>
                    )
                }
            </div>
    }

    useEffect(() => {
        if (imageRef.current != null) {
            const imageTop = imageRef.current.getBoundingClientRect().top
            const pageBottom = window.innerHeight;
            if (imageTop <= pageBottom) {
                set(true)
            } else {
                set(false)
            }
        }
    }, [imageRef])

    return (
        <div ref={imageRef}>
            {jsxSection}
        </div>
    )
}

export default BrowserDiscount