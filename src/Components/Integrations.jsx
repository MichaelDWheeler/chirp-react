import React from 'react'
import Vendors from './Vendors'
import ButtonDemo from './ButtonDemo'

function Integrations(props) {
    return (
        <div className="py-20 sm:py-40 bg-white flex flex-wrap  px-4">
            <h2 className="w-full text-center text-3xl sm:text-3lg md:text-4xl lg:text-5xl xl:text-6xl font-proxima-nova 
            font-bold leading-tight"><span className="text-purple-650">Integrations</span><br></br>The More Data, the Better</h2>
            <div className="flex justify-center w-full">
                <p className="my-8 max-w-screen-xl text-center font-proxima-nova text-lg">
                    chirp&trade; integrates effortlessly into the Shopify ecosystem
                    - allowing for more audience insights and<br></br>an even more personalized online journey for each and every customer.
                </p>
            </div>
            <div className="flex justify-center w-full items-center">
                <Vendors />
            </div>
            <div className="mb-2 flex justify-center w-full">
                <ButtonDemo open={props.open} />
            </div>
        </div>
    )
}

export default Integrations