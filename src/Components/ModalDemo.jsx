import React, { useState } from 'react'
import Modal from 'react-modal';
import Loading from './Loading'

Modal.setAppElement('#root')

const url = "https://airtable.com/embed/shryAtrliWJWbux62?backgroundColor=purple"

function ModalDemo(props) {
    const [iframeLoaded, setLoaded] = useState(false)

    let containerStyle = {
        display: 'none'
    }

    function iframeIsLoaded() {
        setLoaded(true)
    }

    function closeModal() {
        setLoaded(false)
        setTimeout(() => {
            props.closeModal()
        }, 0);

    }

    const customStyles = {
        content: {
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            display: 'flex',
            justifyContent: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
        }
    }

    const iframeStyles = {
        backgroundColor: 'transparent',
        border: 'border: 1px solid #ccc',
        height: '760px',
    }

    if (iframeLoaded) {
        containerStyle.display = 'block'
    } else {
        containerStyle.display = 'none'
    }

    return (
        <>
            <Modal
                isOpen={props.modelState}
                onRequestClose={props.closeModal}
                style={customStyles}
                contentLabel="Book A Demo">
                <Loading isLoaded={iframeLoaded} />
                <div className="max-w-screen-md w-full bg-white rounded font-proxima-nova slide__in-top container__modal-h" style={containerStyle}>
                    <div className="px-4 flex justify-between items-center border-b">
                        <h5 className="text-xl font-medium">Request Info</h5>
                        <button className="opacity-75 text-4xl hover:opacity-100 flex items-center" onClick={closeModal}>&times;</button>
                    </div>
                    <div className="p-4">
                        <iframe className="airtable-embed border border-gray-400 rounded-md" src={url}
                            title="Demo" frameBorder="0" width="100%" style={iframeStyles} onLoad={iframeIsLoaded}></iframe>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default ModalDemo