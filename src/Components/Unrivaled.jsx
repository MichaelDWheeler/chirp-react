import React, { createRef, useEffect, useState } from 'react'
import { useMediaQuery } from 'react-responsive'
import UnrivaledLeft from './UnrivaledLeft'
import UnrivaledRight from './UnrivaledRight'
import techImage from '../Images/tech.png'


function Unrivaled() {
    const isMobile = useMediaQuery({
        query: '(max-width: 640px)'
    })

    let scale = 100

    let section

    let [circleSize, setCircleSize] = useState(100)
    let [backgroundProps, setBackgroundProps] = useState(scale)

    let revealTrigger = createRef()

    let circleEffect = {
        clipPath: `circle(${circleSize}% at 50% 50%)`,
    }

    if (isMobile) {
        section =
            <div className="pb-20 sm:py-40 flex flex-wrap w-full flex-col-reverse sm:flex-row bg-white px-4" style={circleEffect}>
                <UnrivaledLeft />
                <UnrivaledRight />
            </div>
    } else {
        section =
            <div ref={revealTrigger} className="h-screen-150 min-h-2500 bg-white">
                <div className="flex flex-wrap sticky top-80" style={{
                    backgroundImage: `url(${techImage})`,
                    backgroundSize: `${backgroundProps}%`,
                    backgroundPosition: 'center',
                    backgroundRepeat: 'no-repeat',
                    height: 'calc(100vh - 80px)',
                }}>
                    <div className="pb-20 sm:py-40 flex flex-wrap w-full flex-col-reverse sm:flex-row bg-white" style={circleEffect}>
                        <UnrivaledLeft />
                        <UnrivaledRight />
                    </div>
                </div>
            </div >
    }

    function revealComponent() {
        if (revealTrigger != null) {
            setCircleSize(0)
            let componentTop = revealTrigger.current.getBoundingClientRect().top
            let newBackgroundPercent

            if (componentTop <= 80) {
                let percent = (componentTop - 80) * -0.10
                let backgroundPercent = percent * 1.50
                newBackgroundPercent = scale + backgroundPercent
                if (newBackgroundPercent < 50) newBackgroundPercent = 50
                setBackgroundProps(newBackgroundPercent)
            }

            if (componentTop <= 0) {
                let percent = componentTop * -0.10
                if (percent >= 100) percent = 100
                setCircleSize(percent)
            }
        }
    }

    useEffect(() => {
        if (!isMobile) {
            revealComponent()
        }
    })

    return (
        <>
            {section}
        </>
    )
}

export default Unrivaled