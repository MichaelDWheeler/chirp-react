import React from 'react'

function ButtonDemo(props) {
    return (
        <button onClick={props.open}
            className="bg-pink-700 text-white border-solid border-2 border-pink-700
    font-bold py-2 px-4 rounded box-border hover:bg-transparent hover:text-pink-700 
    focus:outline-none cursor-pointer">
            BOOK A DEMO
        </button>
    )
}

export default ButtonDemo