import React from 'react'
import BrowserRecommend from './BrowserRecommend'

function ConquestingLeft() {
    return (
        <div className="flex flex-wrap w-full sm:w-1/2 justify-end">
            <h2 className="block sm:hidden w-full text-3xl font-bold font-proxima-nova leading-tight text-center mt-20 mb-8">Master the Art of Competitive Conquesting</h2>
            <BrowserRecommend />
        </div>
    )
}

export default ConquestingLeft