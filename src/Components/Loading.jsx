import React from 'react'


function Loading(props) {

    let loadedStyle

    if (props.isLoaded) {
        loadedStyle = {
            display: 'none'
        }
    } else {
        loadedStyle = {
            display: 'block'
        }
    }

    return (
        <div className="loading__spinner" style={loadedStyle}></div>
    )
}

export default Loading