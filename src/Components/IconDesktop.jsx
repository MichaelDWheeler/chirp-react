import React from 'react'
import iconDesktop from '../Images/icon_desktop.png'

function IconDesktop() {
    return (
        <img src={iconDesktop} alt="desktop icon" />
    )
}

export default IconDesktop