import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { useTransition, animated } from 'react-spring'
import MobileNav from './MobileNav'
import Logo from './Logo'
import ButtonDemo from './ButtonDemo'

function Header(props) {
    const [showMenu, setMenu] = useState(false)

    const menuTransitions = useTransition(showMenu, null, {
        from: { opacity: 0, transform: 'translateX(-100%)' },
        enter: { opacity: 1, transform: 'translateX(0)' },
        leave: { opacity: 0, transform: 'translateX(-100%)' },
    })

    let menu = null

    if (showMenu) {
        menu = <MobileNav setMenu={setMenu} open={props.open} />
    }

    return (
        <>
            <header className="fixed flex w-full h-20 justify-center border-b border-gray-250 bg-white bg-opacity-75 filter__header z-10">
                <nav className="flex px-6 max-w-screen-xl w-full items-center justify-between">
                    <Link to="/">
                        <Logo />
                    </Link>
                    <FontAwesomeIcon icon={faBars} className="md:hidden lg:hidden xl:hidden cursor-pointer" onClick={() => { setMenu(!showMenu); }} />
                    <ul className="flex items-center font-proxima-nova text-base hidden sm:hidden md:flex lg:flex xl:flex">
                        <li className="mr-12 font-semibold">
                            <Link className="hover:text-pink-700" to="/about">ABOUT</Link>
                        </li>
                        <li>
                            <ButtonDemo open={props.open} />
                        </li>
                    </ul>
                </nav>
            </header>

            {menuTransitions.map(({ item, key, props }) =>
                item &&
                <animated.div
                    key={key}
                    style={props}
                    className="fixed px-4 z-10 top-80 left-0 w-4/5 bg-gray-200 text-black mobile__menu md:hidden">
                    {menu}
                </animated.div>
            )}
        </>
    )
}

export default Header