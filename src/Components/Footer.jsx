import React from 'react'
import { Link } from 'react-router-dom'
import LogoWhite from './LogoWhite'

function Footer(props) {
    let styles = {
        'height': '130px',
        'backgroundImage': 'linear-gradient(to right, #ae308b, #7150db)'
    }
    return (
        <div style={styles} className="flex w-full justify-center items-center">
            <div className="flex px-6 max-w-screen-xl w-full items-center justify-between items-center">
                <LogoWhite />
                <div className="flex flex-wrap justify-end">
                    <p className="text-white font-proxima-nova hover:text-pink-700 cursor-pointer" onClick={props.open}>BOOK A DEMO</p>
                    <p className="w-full text-right">
                        <Link className="text-white font-proxima-nova hover:text-pink-700" to="/about">ABOUT</Link>
                    </p>

                </div>
            </div>
        </div>
    )
}

export default Footer