import React from 'react'
import DesktopLeft from './DesktopLeft'
import DesktopRight from './DesktopRight'

function Desktop(props) {

    return (
        <div ref={props.desktopRef} className="-mt-64 pt-48 md:pt-64 pb-6 bg-gray-250 flex flex-wrap">
            <h2 className="w-full text-center text-3xl sm:text-3lg md:text-4xl lg:text-5xl xl:text-6xl font-proxima-nova 
            font-bold leading-tight"><span className="text-purple-650">Desktop</span><br></br>It's Where Conversions Happen</h2>
            <div className="flex flex-wrap w-full justify-center">
                <div className="flex flex-wrap max-w-screen-xl w-full py-8 md:py-32 px-6">
                    <DesktopLeft />
                    <DesktopRight />
                </div>
            </div>
        </div>
    )
}

export default Desktop