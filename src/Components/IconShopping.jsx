import React from 'react'
import iconShopping from '../Images/icon_shopping.png'

function IconShopping() {
    return (
        <img src={iconShopping} alt="shopping bags icon" />
    )
}

export default IconShopping