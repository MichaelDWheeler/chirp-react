import React from 'react'
import Image from "../Images/browser_platform_V2.png"

function BrowserPlatform() {
    return (<img className="self-center" src={Image} alt="Browser displaying discounts" />)
}

export default BrowserPlatform