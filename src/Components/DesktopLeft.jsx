import React from 'react'
import TopBrands from './TopBrands'


function DesktopLeft() {
    return (
        <div className="flex w-full md:w-1/2 justify-center md:justify-start font-proxima-nova text-lg">
            <div className="max-w-md w-full">
                <p className="text-center md:text-left">
                    Dominant brands saw the potentail in extension technology from ecommerce. They tapped into it a new,
                    fresh way to influence and take control of their customers' journey.
                </p>
                <p className="text-center md:text-left text-purple-650 my-6">
                    chirp&trade; makes it simple for your brand to do the same, except better.
                </p>
                <TopBrands />
            </div>
        </div>
    )

}

export default DesktopLeft

