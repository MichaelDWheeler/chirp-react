import React from 'react'
import Image from "../Images/top-brands.png"

function TopBrands() {
    return (<img className="self-center mx-auto md:mx-0 mb-10" src={Image} alt="Top Brands" />)
}

export default TopBrands