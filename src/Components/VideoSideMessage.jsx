import React from 'react'
import { useTransition, animated } from 'react-spring'

function VideoSideMessage(props) {
    const transitions = useTransition(props.showVideoSideMessage, null, {
        from: { opacity: 0 },
        enter: { opacity: 1 },
        leave: { opacity: 0, visibility: 'hidden' },
    })

    let textContainerStyle = {
        position: 'fixed',
        top: props.top,
        height: '205.33px',
        zIndex: '-1'
    }

    return (
        <div className="hidden sm:block ml-auto sm:h-screen self-end flex-60 lg:flex-50 xl:flex-40">
            <div className="flex flex-wrap self-end justify-start max-w-screen-xl w-full"
                style={textContainerStyle}>
                <div ref={props.videoSideMessageRef} className="ml-8 w-400">
                    {transitions.map(({ item, key, props }) =>
                        item && <animated.div key={key} style={props}>
                            <div>
                                <div className="flex justify-start w-full">
                                    <h2 className="text-5xl font-bold font-proxima-nova leading-tight text-left text-black">
                                        Drive More Sales,<br></br>More Frequently.
                                    </h2>
                                </div>
                                <div className="flex justify-start w-full">
                                    <p className="text-lg font-proxima-nova leading-tight text-left text-black">
                                        Supercharge your brand's ecommerce game by drastically increasing average order value and purchasing frequency by introducing Right Time Messaging.
                            </p>
                                </div>
                            </div>
                        </animated.div>
                    )}
                </div>
            </div>
        </div>
    )
}

export default VideoSideMessage