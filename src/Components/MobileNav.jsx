import React from 'react'
import { Link } from 'react-router-dom'
import ButtonDemo from './ButtonDemo'

function MobileNav(props) {

    return (
        <nav>
            <ul className="font-proxima-nova text-base" onClick={() => props.setMenu(false)}>
                <li className="mr-12 my-4 font-semibold">
                    <Link to="/">HOME</Link>
                </li>
                <li className="mr-12 my-4 font-semibold">
                    <Link to="/about">ABOUT</Link>
                </li>
                <li>
                    <ButtonDemo open={props.open} />
                </li>
            </ul>
        </nav>
    )
}

export default MobileNav