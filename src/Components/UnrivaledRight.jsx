import React from 'react'
import BrowserPlatform from './BrowserPlatform'

function UnrivaledRight() {
    return (
        <div className="flex flex-wrap w-full sm:w-1/2 justify-end">
            <h2 className="block sm:hidden w-full text-3xl font-bold font-proxima-nova leading-tight text-center mt-20 mb-8">Access Unrivaled First Party Data</h2>
            <BrowserPlatform />
        </div>
    )
}

export default UnrivaledRight