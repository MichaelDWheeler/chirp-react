import React, { createRef, useState, useEffect } from 'react'
import { useTransition, animated } from 'react-spring'
import { useMediaQuery } from 'react-responsive'

function ConquestingRight() {

    const isMobile = useMediaQuery({
        query: '(max-width: 640px)'
    })

    const [show, set] = useState(false)

    const h2Ref = createRef()

    const transitions = useTransition(show, null, {
        from: { transform: "translateX(100%)" },
        enter: { transform: "translateX(0)" },
        leave: { transform: "translateX(100%)" },
    })

    let jsxSection

    if (isMobile) {
        jsxSection =
            <div>
                <h2 className="hidden sm:block text-3xl md:text-5xl font-bold font-proxima-nova leading-tight text-left text-black">Master the Art of Competitive Conquesting</h2>
                <p className="mt-8 sm:mt-0 text-center sm:text-left">Consistently engage desktop shoppers as they browse anywhere online with unparalleled messaging capabilities through your branded Shopping Assistant.</p>
            </div>
    } else {
        jsxSection =
            <div ref={h2Ref}>
                {
                    transitions.map(({ item, key, props }) =>
                        item && <animated.div key={key} style={props}>
                            <>
                                <h2 className="hidden sm:block text-3xl md:text-5xl font-bold font-proxima-nova leading-tight text-left text-black">Master the Art of Competitive Conquesting</h2>
                                <p className="mt-8 sm:mt-0 text-center sm:text-left">Consistently engage desktop shoppers as they browse anywhere online with unparalleled messaging capabilities through your branded Shopping Assistant.</p>
                            </>
                        </animated.div>
                    )
                }
            </div>
    }

    useEffect(() => {
        if (h2Ref.current != null) {
            const h2RefTop = h2Ref.current.getBoundingClientRect().top
            const pageBottom = window.innerHeight;
            if (h2RefTop <= pageBottom - 250) {
                set(true)
            } else {
                set(false)
            }
        }
    }, [h2Ref])

    return (
        <div className="flex w-full sm:w-1/2 items-center">
            <div className="w-full sm:w-360 ml-4 sm:ml-16 lg:ml-32 xl:ml-248">
                {jsxSection}
            </div>
        </div>
    )
}

export default ConquestingRight