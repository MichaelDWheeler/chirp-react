import React from 'react'
import Image from '../Images/chirp_Logo-white.png'

function LogoWhite() {
    return (<img className="self-center" src={Image} alt="Chirp Logo" />)
}

export default LogoWhite