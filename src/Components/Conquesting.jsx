import React from 'react'
import ConquestingLeft from './ConquestingLeft'
import ConquestingRight from './ConquestingRight'

function Conquesting() {
    return (
        <div className="pb-20 sm:py-40 bg-gray-250 flex flex-wrap px-4">
            <div className="flex flex-wrap w-full">
                <ConquestingLeft />
                <ConquestingRight />
            </div>
        </div>
    )
}

export default Conquesting