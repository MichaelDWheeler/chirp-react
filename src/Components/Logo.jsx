import React from 'react'
import logo from '../Images/logo.png'

function Logo() {
    return (
        <img className="focus:shadow-outline self-center" src={logo} alt="Chirp Logo" />
    )
}

export default Logo