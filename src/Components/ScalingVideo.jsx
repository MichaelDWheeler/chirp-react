import React from 'react'
import { useMediaQuery } from 'react-responsive'
import { useTransition, animated } from 'react-spring'
import VideoExtensions from './VideoExtensions'
import VideoSideMessage from './VideoSideMessage'

function ScalingVideo(props) {
    const isMobile = useMediaQuery({
        query: '(max-width: 640px)'
    })
    const transitions = useTransition(props.showH2, null, {
        from: { opacity: 0 },
        enter: { opacity: 1 },
        leave: { opacity: 0 },
    })

    let h2Styles = {
        top: '50%',
        left: '50%',
        marginLeft: '-380.5px',
        marginTop: '-120px',
        filter: 'drop-shadow(0px 0px 8px rgba(0, 0, 0, 0.5))',
        zIndex: '-2',
    }

    let scale = 3.1565

    if (isMobile) {
        scale = 1
    } else {
        let distanceFromHeaderBottom = props.desktopBottom - 80

        if (distanceFromHeaderBottom < 0) {
            scale = 3.1565 - (-distanceFromHeaderBottom * 0.002)
            if (scale < 1) scale = 1
            if (scale > 3.1565) scale = 3.1565
        }
    }


    return (
        <div className="flex flex-wrap justify-center relative sm:h-screen-double min__h-video">
            <h2 ref={props.h2Ref}
                className="fixed text-8xl font-bold font-proxima-nova leading-tight text-center text-white"
                style={h2Styles}>
                {transitions.map(({ item, key, props }) =>
                    item && <animated.div key={key} style={props} className="hidden sm:block">Drive More Sales,<br></br>More Frequently</animated.div>
                )}
            </h2>
            <h2 className="block sm:hidden text-3xl font-bold font-proxima-nova leading-tight text-center mt-20 mb-8"><span className="text-purple-650"></span>Drive More Sales,<br></br>More Frequently</h2>
            <div className="mb-16 sm:mb:0 container__scaling-video">
                <VideoExtensions scale={scale} videoRef={props.videoRef} showH2={props.showH2} />
                <div className="hidden sm:flex flex-60 lg:flex-50 xl:flex-40"></div>
            </div>
            <VideoSideMessage
                videoSideMessageRef={props.videoSideMessageRef}
                top={props.top}
                showVideoSideMessage={props.showVideoSideMessage} />
        </div>
    )
}

export default ScalingVideo