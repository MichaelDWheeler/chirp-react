import React, { useState, useEffect } from 'react'
import Header from './Components/Header'
import ModalDemo from './Components/ModalDemo'
import About from './Views/About'
import Home from './Views/Home'
import useHomeEffects from './Hooks/useHomeEffects'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"

function App() {
  const {
    bottomPos, setPos, desktopRef, getDesktopBottom, showH2, videoRef, h2Ref, videoH2Status,
    displayVideoSideMessage, top, showVideoSideMessage, videoSideMessageRef, } = useHomeEffects()

  const [modalIsOpen, setIsOpen] = useState(false)

  function closeModal() {
    setIsOpen(false)
  }

  function openModal() {
    setIsOpen(true)
  }

  useEffect(() => {
    window.addEventListener("scroll", function () {
      if (desktopRef.current != null) {
        getDesktopBottom()
        videoH2Status()
        displayVideoSideMessage()
      }
    })
  })

  return (
    <div className="App">
      <ModalDemo modelState={modalIsOpen} closeModal={closeModal} />
      <Router>
        <Header open={openModal} />
        <Switch>
          <Route exact path="/">
            <Home
              desktopRef={desktopRef}
              h2Ref={h2Ref}
              videoRef={videoRef}
              videoSideMessageRef={videoSideMessageRef}
              open={openModal}
              setPos={setPos}
              desktopBottom={bottomPos}
              showH2={showH2}
              top={top}
              showVideoSideMessage={showVideoSideMessage} />
          </Route>
          <Route path="/about">
            <About />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
